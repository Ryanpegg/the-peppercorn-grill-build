<?php
$pageName = 'The Market Place';
$pageFile = 'market';
$description = '';
$keywords = '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title><?php print($pageName); ?></title>
    <meta name="desscription" content="<?php print($description); ?>"/>
    <meta name="keywords" content="<?php print($keywords); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?php
    include_once './resources/pages/content/style_and_script.php';
    ?>
</head>
<body>
<?php
// Include the header for the pages
include_once './resources/pages/content/header.php';
// Include the banner
include_once './resources/pages/content/banner.php';
// Include the navigation
include_once './resources/pages/content/navigation.php';

$GLOBALS['side_images'] = [
    ['market/image_1.png', ''],
    ['market/image_2.png', ''],
    ['market/image_3.png', ''],
    ['market/image_4.png', ''],
    ['market/image_5.png', ''],
    ['market/image_6.png', ''],
    ['market/image_7.png', ''],
    ['market/image_8.png', ''],
    ['market/image_9.png', ''],
    ['market/image_10.png', ''],
    ['market/image_11.png', ''],
    ['market/image_12.png', ''],
    ['market/image_13.png', ''],
    ['market/image_15.png', ''],
    ['market/image_16.png', '']
];

// Include the content section
include_once './resources/pages/content/content.php';
//Include the footer
include_once './resources/pages/content/footer.php';
?>
</body>
</html>
