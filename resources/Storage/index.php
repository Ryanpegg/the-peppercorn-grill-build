<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>The Peppercorn Grill Limited</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<!-- Stylesheets -->
		<link rel="stylesheet" type="text/css" href="./resources/CSS/grid-style.css"/>
		<link rel="stylesheet" type="text/css" href="./resources/CSS/main-style.css"/>
		<!-- Script Files -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="./resources/JS/menu.js"></script>
		<script src="./resources/JS/banner.js"></script>
	</head>
	<body>
		<header>
			<div class="row">
				<div class="column xsmall-12">
					<a href="./" class="logo logo-heading">
						<!-- Style with CSS -->
						<span>The Peppercorn Grill Limited</span>
					</a>
				</div>
				<div class="column xsmall-12">
					<!-- Social Media section -->
				</div>
				<div class="column xsmall-12 menuButton">
					<!-- Menu button for JS based navigation -->
					<a href="#" class="menubutton">
						<!--
							use html & css to build the menu button. This will reduce the number of images
							to be sent accross the internet. Could reduce the page load time
						-->
						<div>
							<div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</header>
		<div class="banner banner-landing">
			<!-- This will change in a slideshow -->
			<div class="image">
				<div class="holder">
					<div class="taglines">
						<div>
							<h1>Welcome to The Peppercorn Grill</h1>
							<p>Tagline to be added here...</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav>
			<div class="closeMenu">
				<a href="#" class="closeMenu">
					<div>
						<div>
							<span>X</span><!-- Style with CSS -->
						</div>
					</div>
				</a>
			</div>
			<ul>
				<li><a href="./">Home</a></li>
				<li><a href="./content.php">About</a></li>
				<li><a href="./location.php">Location</a></li>
				<li><a href="./menu.php">Menu</a></li>
				<li><a href="./contact.php">Contact Us</a></li>
			</ul>
		</nav>
		<div class="content">
			<div class="row">
				<article class="landing-content">
					<!-- Page content will be placed here -->
					<h2>The Peppercorn Grill promise to serve you only the most oustanding, ingredients and food.</h2>
					<p>We aim to serve you wherever you are, delivering the very best of lunches and no less, in it’s customer service.  We create  the most innovative, freshest, finest meals and offer you the highest value for money products that money can buy.</p>
					<h3>At the Shopping Mall</h3>
					<p>When  you're out and about shopping with the focus being on buying the one, all important gift or have heaps to do and get but with little time to fit lunchtime leaving very  little time to go to a restaurant, order a meal and enjoy it.  The peppercorn Grill will offer you a f fresh flavoursome, value for money alternative when you’re hungry and short on time.  With the meals  appropriately packed allowing you to walk around and enjoy your meal whilst shopping at the same time.  Save on time and still enjoy a flavoursome, value for money meal.</p>
					<p>But please, don't just take our word for it, try a meal yourself.</p>
				</article>
			</div>
		</div>
		<footer>
			<div class="row">
				<div class="column xsmall-12 large-6">
					<a href="./" class="logo logo-footer">
						<span>The Peppercorn Grill Limited</span>
					</a>
					<p>&copy; The Peppercorn Grill Limited 2018</p>
					<p>Company Registration: 10890159</p>
				</div>
				<div class="column xsmall-12 xsmall-hide large-6 large-show large-right">
					<!-- Smaller navigation and  social media-->
					<nav class="smaller">
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">Location</a></li>
							<li><a href="#">Menu</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</footer>
	</body>
</html>
