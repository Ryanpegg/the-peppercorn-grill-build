<h2>Get in Touch</h2>

<p>Email <a href="mailTo:Sabina@thepeppercorngrill.co.uk">Sabina@thepeppercorngrill.co.uk</a></p>
<p>Alternatively you can phone us on <a href="tel:+447958550431">+44 7958 550 431</a></p>
<p>We’d love to hear from you ~</p>

<?php
  $errors = array();
  $name = '';
  $email = '';
  $subject = '';
  $message = '';

  function test_data($input){
    $input = trim($input);
    $input = stripslashes($input);
    $input = htmlspecialchars($input);
    return $input;
  }

  if(isset($_POST['submitted'])){
    $name = test_data($_POST['name']);
    $email = test_data($_POST['email']);
    $subject = test_data($_POST['subject']);
    $message = test_data($_POST['mess']);

    //Check the name
    if(empty($name)){
      array_push($errors, 'Please enter your name.');
    }

    //check the email address
    if(empty($email)){
      array_push($errors, 'Please enter your email address.');
      //check for a valid email address
      if(filter_var($email, FILTER_VALIDATE_EMAIL)){
        //email is valid
      } else {
        array_push($errors, 'Please enter a valid email address.');
        $email = '';
      }
    }

    //check the subject has been entered
    if(empty($subject)){
      array_push($errors, 'Please enter a subject.');
    }

    if(empty($message)){
      array_push($errors, 'Please enter a message.');
    }

    if(count($errors) == 0){
      $to = 'Sabina@thepeppercorngrill.co.uk';
      $date = date('d/m/Y');
      $emailsubject = 'CONTACT FORM: ' . $subject . ' - ' . $date;

      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= "From: " . $email . "\r\n";

      $content = "<html><head><title>" . $emailsubject . "</title></head><body><p><b>Name:</b> " . $name . "</p><p><b>Email:</b> <a href=\"mailTo:" . $email . "\">" . $email . "</a></p><p><b>Subject: </b> " . $subject . "</p><p><b>Message:</b> " . $message . "</p></body></html>";

      if(mail($to, $emailsubject, $content, $headers)){
        print('<h3>Thank you.</h3><p>Somebody will be in touch as soon as possible.</p>');
      } else {
        array_push($errors, 'Form failed to send. Please try again. If this issue persists please get in touch using an alternative method.');
      }
    }
  }

  if(count($errors) > 0 || !isset($_POST['submitted'])){
 ?>

<?php

  print('<div class="errors">');
  for($i = 0; $i < count($errors); $i++){
    $errorMessage = $errors[$i];
    print('<div class="error">' . $errorMessage . '</div>');
  }
  print('</div>');

 ?>

<form action="./contact.php" method="post">
    <div class="input">
        <label for="contactname">Your Name:</label>
        <input type="text" name="name" id="contactname" placeholder="John Smith" value="<?php print($name); ?>"/>
    </div>
    <div class="input">
        <label for="email">Your Email:</label>
        <input type="email" name="email" id="email" placeholder="johnsmith@example.co.uk" value="<?php print($email); ?>"/>
    </div>
    <div class="input">
        <label for="subject">Subject:</label>
        <input type="text" name="subject" id="subject" placeholder="eg. Ordering" value="<?php print($subject); ?>"/>
    </div>
    <div class="input input-text">
        <label for="mess">Your Message:</label>
        <textarea name="mess" id="mess" placeholder="Enter your message here..."><?php print($message); ?></textarea>
    </div>
    <div class="buttons">
        <button type="submit" name="submitted">
            Send
        </button>
        <button type="reset">
            Clear
        </button>
    </div>
</form>

<?php
}
 ?>
