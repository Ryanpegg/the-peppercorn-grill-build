<div class="content">
    <div class="row">
        <article class="column xsmall-12 large-8 large-left">
            <!-- Page content will be placed here -->
            <?php
                include_once './resources/pages/' . $pageFile . '.php';
            ?>
        </article>
        <div class="column xsmall-12 large-4">
            <aside class="sideContent">
                <?php
                    include_once './resources/pages/content/side_content.php';
                ?>
            </aside>
        </div>
    </div>
</div>