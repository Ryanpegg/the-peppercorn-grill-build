<div class="banner banner-content">
    <!-- This will hold the page name -->
    <div class="backgroundImage preload">
        <div>
            <img src="./resources/IMG/banners/couscous.jpg" alt="Couscous Image"/>
        </div>
    </div>
    <div class="backgroundImage view">
        <div>
            <img src="./resources/IMG/banners/couscous.jpg" alt="Couscous Image"/>
        </div>
    </div>
    <div class="mist">
        <div>
            <div>
                <h1><?php print($pageName); ?></h1>
            </div>
        </div>
    </div>
</div>