<header>
    <div class="row">
        <div class="column xsmall-12">
            <a href="./" class="logo logo-heading">
                <!-- Style with CSS -->
                <span>The Peppercorn Grill Limited</span>
            </a>
        </div>
        <div class="column xsmall-12">
            <!-- Social Media section -->
        </div>
        <div class="column xsmall-12 menuButton">
            <!-- Menu button for JS based navigation -->
            <a href="#" class="menubutton">
                <!--
                    use html & css to build the menu button. This will reduce the number of images
                    to be sent accross the internet. Could reduce the page load time
                -->
                <div>
                    <div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</header>