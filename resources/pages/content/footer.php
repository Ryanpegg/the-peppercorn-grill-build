<footer>
    <div class="row">
        <div class="column xsmall-12 large-6">
            <a href="./" class="logo logo-footer">
                <span>The Peppercorn Grill Limited</span>
            </a>
            <p>&copy; The Peppercorn Grill Limited 2018</p>
            <p>Company Registration: 10890159</p>
        </div>
        <div class="column xsmall-12 xsmall-hide large-6 large-show large-right">
            <!-- Smaller navigation and  social media-->
            <nav class="smaller">
                <ul>
                    <?php
                        include './resources/pages/content/page_list.php';
                    ?>
                </ul>
            </nav>
        </div>
    </div>
</footer>
