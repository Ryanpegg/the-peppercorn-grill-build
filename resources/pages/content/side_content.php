<div class="imagesBoard">
<?php
/**
 * Created by PhpStorm.
 * User: Pegg
 * Date: 23/01/2018
 * Time: 12:14
 */

// create the boxes from the list
$images = [
        ['breakfast-2408034_1920.jpg', 'Breakfast Sandwich'],
        ['sandwich-1580353_1920.jpg','Sandwich'],
        ['couscous-1503949_1920.jpg','Couscous'],
        ['kebab-2052498_1920.jpg','Kebab'],
        ['sandwich-2897901_1920.jpg','Sandwich'],
        ['rice-389289_1920.jpg','Rice'],
        ['grilled-chicken.jpg', 'Grilled Chicken'],
        ['meat.jpg', 'Meat Kebabs'],
        ['steak.jpg', 'Fresh Steak'],
        ['vegetable-skewer.jpg', 'Vegetable Skewered']
];

if ($GLOBALS['side_images']) {
    $images = $GLOBALS['side_images'];
}

//loop to select 6

function buildImages($selected, $pictures){
    for($idx = 0;$idx < count($selected);$idx++){
        //print($selected[$idx]);
        if($idx <= count($pictures) && $idx >= 0){
            $select = $selected[$idx];
            print('<div class="imageBox"><div><div><img src="./resources/IMG/PCG-Images/' . $pictures[$select][0] . '" alt="' . $pictures[$select][1] . '"/></div></div></div>');
        }
    }
}

function selectImages($selected, $pictures){
    if(count($selected) >= count($pictures)){
        buildImages($selected, $pictures);
    } else {
        $select = mt_rand(0, count($pictures) - 1);
        if ($select <= count($pictures) - 1 && $select >= 0) {
            if (!in_array($select, $selected)) {
                //print($select);
                array_push($selected, $select);
            }
        }
        selectImages($selected, $pictures);
    }

}

selectImages([], $images);

?>
</div>
<div class="cover">
    <div class="imageDisplay">
        <div class="closeCover">
            <a href="#" class="close">
                <div>
                    <div>
                        <span>X</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="view">
            <img src="./resources/IMG/PCG-Images/breakfast-2408034_1920.jpg" alt="Sandwich"/>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.imageBox').css({
            'cursor': 'pointer'
        });

        $('.closeCover > A').on('click', function(e) {
            e.preventDefault();
            $('BODY').removeClass('stop-scrolling');
            $('.cover').fadeOut(400);
            $('.cover').slideUp(400);
        });

        $('.imagesBoard > .imageBox').on('click', function(e){
            e.preventDefault();
            var image = $(this).children('div').children('div').children('IMG');
            var href = image.attr('src');
            var alt = image.attr('alt');
            $('.cover > .imageDisplay > .view > IMG').attr('src', href);
            $('.cover > .imageDisplay > .view > IMG').attr('alt', alt);
            $('BODY').addClass('stop-scrolling');
            $('.cover').fadeIn(400);
            $('.cover').slideDown(400);
        });

        $('.cover').on('click', function(e){
            e.preventDefault();
            $('BODY').removeClass('stop-scrolling');
            $('.cover').fadeOut(400);
            $('.cover').slideUp(400);
        }).on('click', 'DIV.imageDisplay', function(e){
            e.stopPropagation();
        })
    });
</script>
