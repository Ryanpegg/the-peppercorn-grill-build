<div class="banner banner-landing">
    <!-- This will change in a slideshow -->
    <div class="image">
        <div class="holder">
            <div class="taglines">
                <div>
                    <h1>Welcome to The Peppercorn Grill</h1>
                    <p>Bringing your lunch to you...</p>
                </div>
            </div>
        </div>
    </div>
</div>