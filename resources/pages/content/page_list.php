<?php
    $pages = [['', 'Home'], ['market', 'The Market Place'], ['office_lunches', 'Office &amp; University Lunches'], ['the_grill', 'The Grill'], ['contact', 'Contact Us']];
    for($idx = 0; $idx < count($pages); $idx++){
        $class = '';
        $href = $pages[$idx][0];
        $name = $pages[$idx][1];
        if($pageName == $pages[$idx][1]){
            $class = 'active';
        }

        if($href != ''){
            $href = $href . '.php';
        }
        $href = './' . $href;
        print('<li class="' . $class . '"><a href="' . $href . '">' . $name . '</a></li>');
    }
?>
