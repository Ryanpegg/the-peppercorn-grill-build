<!-- Page content will be placed here -->
<h2 class="centre_underline">The Peppercorn Grill promise to serve you only the finest ingredients and indeed, lunch</h2>
<p>The Peppercorn Grill specialise in getting lunch to you wherever you are.  We create the most innovative and exciting meals that make you really look forward to lunch time.  Lunchtime is a very important time, if you want to complete the rest of your day with a good frame of mind and in a good mood too, you need lunch.</p>
<p>At the Peppercorn Grill we make it our priority and no less promise to serve you the very best of lunches starting by using the very  finest,   freshest  ingredients, created by the most experienced hands creating the most beautiful meals and all at excellent prices.</p>
<p>But don't take our word for it, try a meal yourself ~ We look forward to serving you.</p>
