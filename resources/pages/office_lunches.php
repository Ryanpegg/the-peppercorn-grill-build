<h2>Office &amp; University Lunches</h2>
<p>With the ever increasing heavy and never ending workload, which all too often leaves you little time to head out for a meal and wait in long queues to get one and even less time to eat and enjoy a good, wholesome lunch. The Peppercorn Grill would like to introduce you to the solution to these lunchtime problems by supplying you with delicious, wholesome, fresh, FILLING and nutritious meals / lunches, jam packed with the very finest of ingredients directly to you at the comfort of your own desk in your office. The Peppercorn Grill will supply meals that will be delivered fresh and hot. All of this in the name of giving you (STAFF or STUDENT) a full hour to relax and enjoy a delicious lunch allowing you time to fully recharge your batteries for the rest of the day.</p>
<br />
<h3>Sample Menu</h3>
<?php
// Create the menu using array, which could become a DB connection
$menuItems = [
  ['Chicken & Seafood Wrap', 'Delicately infused strips of chicken topped with a layer of aromatic seafood, mushrooms and sweetcorn, finished with a sweet & hot mango chutney'],
  ['Roast Lamb', 'Roast lamb infused in garlic and chilli, slowly roasted on a layer of silver onions and green peppers. Served on a bed of Couscous and green salad. Finished with a sauce of your choice.'],
  ['Stuffed Peppers', 'Stuffed peppers stuffed with the most aromatic minced meat and potatoes finished with fine, authentic herbs and spices\', ~ Served on a bed of fluffy, fragrant rice'],
  ['Spicy Meatballs ', 'Spicy minced meat marinated in fine, aromatic, authentic herbs and spices, creating lush sauce. Finished with a generous sprinkling of cheese. This can be served in a rustic bread rolls or on a bed or rice/couscous'],
  ['Haddock Fillet Wrap', 'Fillet of delicately flavoured haddock served on a bed of spicy, roast onions and spinach ~ Served on fresh, rustic bread'],
  ['Roast Cod', 'Lightly marinated Cod, served a bed of emmental Cheese, roast tomatoes and generously topped with a Sweet Chilli  sauce ~ Served on fresh, rustic bread'],
  ['Marinated Prawns', 'Soft, sweet prawns marinated in a light ginger & soy sauce, laid on a bed of mushrooms and showered with chopped olives and sweetcorn and finished with a layer of mozzarella cheese ~ Served on fresh, rustic bread.'],
  ['Roasted Vegetables', 'Delicately roasted in herbs and married with the finest haloumi cheese ~ Served on a fresh, rustic bread'],
  ['Creamy Brie', 'Generously showered with crushed walnuts and black grapes, served on fresh, rustic bread'],
  ['Chickpeas', 'Chickpeas prepared in the authentic, spicy Bombay style ~ Served with rice and and delicious, lush salad.'],
  ['Curry Pot', 'Classic Chicken, Lamb, Beef or Veal curry prepared by using the most authentic and rich herbs and spices and prepared by the most experienced and expert hands ~ Served on a bed of fluffy, flavoursome rice and finished with a light salad and pickle of choice'],
];

print('<dl class="menu">');
for($idx = 0; $idx < count($menuItems); $idx++){
    print('<dt><div class="border"><h4>' . $menuItems[$idx][0] . '</h4></div>');
    print('<dd>');
    print('<p>' . $menuItems[$idx][1] . '</p>');
    if (count($menuItems[$idx]) > 2) {
      print('<div><img src="' . $menuItems[$idx][2] . '" alt="' . $menuItems[$idx][3] . '"/></div>');
    }
    print('</dd>');
}
print('</dl>');
?>
<p style="text-transform: uppercase"><b><i>* THE ABOVE IS A SAMPLE MENU, OUR VARIETY/ES ARE HUGE AND GROWING ALL THE TIME.  ALL MEALS ARE GENEROUSLY FINISHED WITH A CHOICE OF SAUCES.</i></b></p>
<br />
<p style="text-transform: uppercase"><b><i>HOLD ON, BECAUSE WE DON'T STOP THERE.  IF YOU WOULD LIKE A PARTICULAR MEAL MADE FOR YOURSELF AND A GROUP OF FRIENDS (15+), LET US KNOW.  WE WOULD LOVE TO MAKE YOUR MEAL YOUR WAY. THIS MAY REQUIRE PRE-PAYMENT OF THE FOOD/S.</i></b></p>
<br />
<h3>Another Thing.......!</h3>
<p>University lunches will be prepared a bit differently.  Meals will be prepared when you tell us what you would like to eat the following day.  Meals will be prepared in accordance to a daily consensus.  We will leave 3 boxes at the service area ~ one for vegetarian, non~vegetarian and seafood.  Students will simply need to slip in the box a meal of their choice for the following day.  Meals will then be prepared as per the highest votes.  In this way we hope largely, to prepare the foods you, the students would like to eat.</p>
<br />
<h3>HOLD ON HOLD ON…………...WE’RE NOT DONE YET!</h3>
<p>Do you sometimes struggle with what to eat for dinner, feel like eating something, don’t have the ingredients even if you did, you just don’t have the time! Well the PCG can offer you a solution for this too.  We will supply your dinner to you when you purchase your lunch but we’ll pack it so that it is fully sealed and microwaveable for when needed ~ your dinner will be ready in no time at all.</p>
<br />
<h2>Need lunch for an office conference/meeting?</h2>
<p>We are pleased to be able to extend our catering services further to any private meetings, conferences or office functions taking place within your office/establishment. Simply get in touch with staff at the PCG. Staff will be at hand, happy to advise and talk you through any decisions needed to be made for the event.</p>
<p>Whether you require a buffet / finger or full lunch and a listed selection of meals / buffet items will be sent to you allowing you to select the items you require for the day in your own time.</p>
<p>Here too, The Peppercorn Grill are pleased to be able to cater for individual diets including, diabetic, gluten free, etc. Just let us know what your requirements are and leave the rest to us.</p>
<p>Ordering buffet/lunch for the conference / meeting really couldn’t be any easier.</p>
<p>The PCG have dedicated staff on hand, who will be more than happy to help / advise and book all your requirements. Simply request a list of either buffet items / lunch items, you can then either go through the list with staff at the PCG or take your time to decide what you would like best. Please remember, staff will always be on hand to advise and assist you. All bookings need to be confirmed by at least one week before delivery is required, however if this is required in a hurry owing to cancelation etc, we will most certainly help you out with this sooner.</p>
<br />
<script src="./resources/JS/hideReveal.js"></script>
