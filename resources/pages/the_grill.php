<h2>The Grill</h2>
<p>This is The Peppercorn Grill’s primary speciality.  Owing to specific legal issues and requirements, however, can only be produced and supplied if preparation of these food/s meet with the individual markets / stands legal requirements.</p>
<br />
<h3>Menu for the barbecue</h3>
<?php
    // Create the menu using array, which could become a DB connection
    $menuItems = [
      ['Marinated Lamb', 'Lamb gently marinated in authentic spices and infused in chilli oil for over 48 hours, Served on a bed of fresh green salad, olives and spring onions. Topped with a sauce / chutney of individual choice ~ Served in fresh, fluffy pitta bread'],
      ['Lamb / Beef Steaks', 'Succulent, juicy steaks prepared in a buttery, garlic and fresh herb dressings ~ Served in a Baked potato topped with a choice of toppings'],
      ['Lamb Chops', 'Oozing with spices straight off the Indian spice rack, marinated for over 24 hours, dressed in a fine, spicy sauce.  Served on a bed of fluffy Basmatti rice '],
      ['Lamb on skewers', 'Succulent chunks of juicy lamb and fresh chunky vegetables marinated for 24 hours in pure, authentic Indian spices and dressed with sweet chilli sauce and a choice of  lush fresh salads ~ all served in a fresh, fine Pitta bread'],
      ['Veal on skewers', 'Juicy Veal dressed with fine, fresh vegetables, beautifully marinated in garlic and chilli, dressed with friend onions and olives ~  all served in a fresh, fine Pitta bread'],
      ['Spicy Beef', 'Beef marinated in fresh herbs and authentic spices. Cooked to required time and dressed with the finest home made coleslaw and finished with raw, sweet chilli ~ Served in fresh, fluffy pitta bread'],
      ['Venison Steaks', 'Succulent, juicy and well seasoned in a light chilli, peppery sauce ~ Served on a bed of couscous and green salad with olives and finished with fine grain mustard.'],
      ['T bone steak', 'Quite simply steak marinated in the most flavoursome, chilli olive oil and garlic ~ Served either with Jacket potatoes, smothered with a topping of one’s choice or on a bed of flavoursome couscous and lush, green salad!'],
      ['Chicken', 'Chicken marinated in a fine, light tandoori marinade, dressed with mint yogurt and a greed salad. ~ Served on a bed or fluffy Basmatti rice or Couscous', '/resources/IMG/PCG-Images/chicken-4493690_640.jpg', 'A picture of chicken'],
      ['Chicken variation 2', 'Half a baby chicken marinated in garlic, ginger, ground chilli and honey.'],
      ['Chicken variation 3', 'Half a baby chicken marinated for 24 hours in nothing but Authentic Tandoori spices'],
      ['Chicken variation 4', 'Half a baby chicken marinated in a rich spicy and lush coconut marinade', '/resources/IMG/PCG-Images/barbecue-84671_640.jpg', 'A picture of chicken on a barbecue'],
      ['Lamb & Pepper (lamb chunk) Kebabs', 'Lamb marinated in authentic spices for 24 hours finely prepared with green peppers ~ served with a choice of coleslaw / mixed salad / green salad and a sauce/ chutney of individual choice ~ Served in a fresh fluffy pitta bread', '/resources/IMG/PCG-Images/shish-kabob-6335707_640.jpg', 'A picture of kebabs'],
      ['A medley of vegetables', 'A medley of vegetables marinated and grilled to perfection ~ Served in fresh, soft naan bread.'],
      ['Aubergine', 'Marinated in authentic herbs and spices, baked and stuffed with spicy rice and finished with a dressing of the individual’s choice.', '/resources/IMG/PCG-Images/rice-6697956_640.jpg', 'A picture of rice']
    ];

    print('<dl class="menu">');
    for($idx = 0; $idx < count($menuItems); $idx++){
        print('<dt><div class="border"><h4>' . $menuItems[$idx][0] . '</h4></div>');
        print('<dd>');
        print('<p>' . $menuItems[$idx][1] . '</p>');
        if (count($menuItems[$idx]) > 2) {
          print('<div><img src="' . $menuItems[$idx][2] . '" alt="' . $menuItems[$idx][3] . '"/></div>');
        }
        print('</dd>');
    }
    print('</dl>');
?>
<br />
<h3>Variation Two</h3>
<p>Where the above barbecue grilling is not possible however, the below items will be served and will be just as delicious.  All the items including the above and below are all expertly created by hands with years of experience and culinary skill and knowledge.</p>
<?php
    // Create the menu using array, which could become a DB connection
    $menuItems = [
      ['Spicy Meatballs', 'Spicy minced lamb marinated and cooked in a rich yoghurty sauce and authentic Indian spices, layered on a bed of fluffy  fragrant rice and finished with a choice of fresh salads'],
      ['Classic Chicken curry ', 'Prepared with the most Authentic of Indian  spices, including freshly ground garam masala. Served with potatoes on a bed of fluffy Basmatti rice and served with a choice of salads'],
      ['Classic Lamb curry', 'Choicest chunks of lamb prepared in Authentic Indian spices and finished with a dressing of Corriander and toasted onions ~ Served on a bed of fluffy Basmatti rice'],
      ['Stuffed Green Peppers (curry)', 'Whole green peppers stuffed with flavoursome minced meat and potatoes, prepared using the most Authentic of spices  finished in a light curry and served on a bed of fluffy, flavoursome rice and light salad'],
      ['Prawns in a coconut sauce (curry)', 'Prawns prepared in a rich, lush coconut sauce and dressed with spring onions and sweet chilli. Served on a bed of fluffy, fragrant rice.'],
      ['Aubergine & Bean Curry', 'Prepared in fine, authentic spices, delicately laced with a tinge of ginger & garlic'],
      ['Bombay Potato Wrap', 'Potatoes cooked the Bombay style, prepared in olive oil, lightly spiced with chilly, turmeric and lightly fried in poppy seeds. Finished with a layer of rich, flavoursome daal, all wrapped up in a freshly made chappatti.'],
      ['A Selection of Dahl', 'A selection of the most fragrant, aromatic daal\'s including: Chana dahl, Toor dahl, masoor, Mung dahl and so much more']
    ];

    print('<dl class="menu">');
    for($idx = 0; $idx < count($menuItems); $idx++){
        print('<dt><div class="border"><h4>' . $menuItems[$idx][0] . '</h4></div>');
        print('<dd>');
        print('<p>' . $menuItems[$idx][1] . '</p>');
        if (count($menuItems[$idx]) > 2) {
          print('<div><img src="' . $menuItems[$idx][2] . '" alt="' . $menuItems[$idx][3] . '"/></div>');
        }
        print('</dd>');
    }
    print('</dl>');
?>

<p style="text-transform: uppercase"><b><i>* WE ALSO CATER FOR MAJOR DIETS INCLUDING, VEGAN, VEGETARIAN, GLUTEN FREE. AT THE MARKET PLACE, MEALS WILL BE SERVED ON A CHOICE OF A BED OF FRAGRANT BASMATI RICE OR COUSCOUS. SERVED WITH A LIGHT SALAD, LACED WITH A CHOICE OF DRESSINGS INCLUDING MUSTARD, CHILLI, COCONUT SAUCE AND MORE</i></b></p>
<script src="./resources/JS/hideReveal.js"></script>
