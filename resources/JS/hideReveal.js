//function to hide all
function hideAll(){
    var DTTarget = $('DL.menu > DT');
    var DDTarget = $('DL.menu > DD');

    DDTarget.slideUp(500);
    DTTarget.addClass('hidden', 500);
}

//function for hide/Reveal
function hideReveal(item){
    //By default apply to all
    var target = $('DL.menu > DT');
    //The operation being done 1=open and 0=closed
    var op = 0;
    //If the target have been entered set it as the target
    if (item != undefined) {
        target = item;
        // look at the target and see if it is open or closed
        if(target.hasClass('hidden')){
            op = 1;
        }
    }

    if(op == 0) {
        target.next('DD').slideUp(500);
        target.addClass('hidden', 500);
    } else {
        hideAll();
        target.next('DD').slideDown(500);
        target.removeClass('hidden', 500);
    }

}

$(document).ready(function(){
    //close all to start off with
    hideAll();

    $('DL.menu > DT').on('click', function(e){
        e.preventDefault();
        hideReveal($(this));
        //alert($(this).html());
    })
})