// All the images, taglines and headings are stored in here
var items = [
    ['Serving You The Freshest and Finest Foods...', 'fruit-3304977.jpg', ''],
    ['From breads', 'baguette-1870221_1920.jpg', ''],
    ['to meat', 'steak-2272464_1920.jpg', ''],
    ['to seafood', 'prawns-2348933.jpg', ''],
    ['to vegetables', 'veg.jpg', ''],
    ['to you!', 'the-conference-3248255_1920.jpg', ''],
    ['Welcome to the Peppercorn Grill', 'rice-389289_1920.jpg', 'Bringing your lunch to you']
];

var banner_images = ['fruit', 'baguette', 'meat', 'prawns', 'veg', 'office', 'rice'];

//targets
var box = $('.taglines > DIV');

//to change the header of the banner
function banner(idx){
    //targets for the content banner
    var contentTarget = $('DIV.banner-content');
    var contentPre = contentTarget.children('.preload');
    var contentView = contentTarget.children('.view');


    setTimeout(function(){
        $('DIV.banner-landing').attr('style', '');
        $('DIV.banner-landing').attr('style', 'background-image: url(\'./resources/IMG/PCG-Images/' + items[idx][1] + '\')');
        contentPre.children('DIV').children('IMG').attr('src', './resources/IMG/banners/' + banner_images[idx] + '.jpg');
        $('.image').fadeOut(500, function(){
            $('DIV.banner-landing > .image > .holder > .taglines > DIV > H1').html(items[idx][0]);
            $('DIV.banner-landing > .image > .holder > .taglines > DIV > P').html(items[idx][2]);
            $('DIV.banner-landing > .image > .holder').fadeOut(0);
            $('.image').attr('style', 'background-image: url(\'./resources/IMG/PCG-Images/' + items[idx][1] + '\')');
            //alert(lines[idx]);
            $('.image').fadeIn(600, function(){
                $('DIV.banner-landing > .image > .holder').fadeIn(600);
            });
        });
        //console.log(images[idx])
        //console.log(lines[idx])
        if(idx < items.length - 1){
            banner(idx + 1);
        } else {
            banner(0)
        }

        // This is for the content pages
        contentView.fadeOut(1000, function(){
            contentView.children('DIV').children('IMG').attr('src', './resources/IMG/banners/' + banner_images[idx] + '.jpg');
            contentView.fadeIn(0);
        })
    }, 5000);
}

$(document).ready(function(){
    // Start the banner image
    banner(0);
})
