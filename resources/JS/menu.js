// store the current width so that if it changes then run the code and not when the height changes
var current_width = $(window).width();


// Function to hide and show the content
function menu(state){
	var left_space = '-100%';
	if(state == 1){
		left_space = 0;
        $('BODY').addClass('stop-scrolling');
	} else {
        $('BODY').removeClass('stop-scrolling');
	}

	$('BODY > NAV').animate({
		'margin-left': left_space
	}, 300);
}

//on window resize
$(window).resize(function(){
	if($(window).width() != current_width) {
        if ($(window).width() >= 1100) {
            $('BODY').removeClass('JS');
						$('BODY').removeClass('stop-scrolling');
            menu(1);
        } else {
            $('BODY').addClass('JS');
            menu(0);
        }
    }
    current_width = $(window).width();
})

$(document).ready(function(){
	//If the JS runs add class to the website
	if($(window).width() < 1100){
		$('BODY').addClass('JS');
		//Default the content to be closed
		menu(0);
	}

	$('A.menubutton').on('click', function(e){
		e.preventDefault();
		menu(1);// Open the content
	})

	$('A.closeMenu').on('click', function(e){
		e.preventDefault();
		menu(0);//close the content
	})
})
