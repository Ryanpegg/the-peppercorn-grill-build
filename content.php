<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>The Peppercorn Grill Limited</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<!-- Stylesheets -->
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="./resources/CSS/grid-style.css"/>
		<link rel="stylesheet" type="text/css" href="./resources/CSS/main-style.css"/>
		<!-- Script Files -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="./resources/JS/menu.js"></script>
        <script src="./resources/JS/banner.js"></script>
        <script src="./resources/JS/hideReveal.js"></script>
	</head>
	<body>
		<header>
			<div class="row">
				<div class="column xsmall-12">
					<a href="./" class="logo logo-heading">
						<!-- Style with CSS -->
						<span>The Peppercorn Grill Limited</span>
					</a>
				</div>
				<div class="column xsmall-12">
					<!-- Social Media section -->
				</div>
				<div class="column xsmall-12 menuButton">
					<!-- Menu button for JS based navigation -->
					<a href="#" class="menubutton">
						<!--
							use html & css to build the menu button. This will reduce the number of images
							to be sent accross the internet. Could reduce the page load time
						-->
						<div>
							<div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</header>
		<div class="banner banner-content">
			<!-- This will hold the page name -->
            <div class="backgroundImage preload">
                <div>
                    <img src="./resources/IMG/banners/breakfast.jpg" alt="Breakfast Image"/>
                </div>
            </div>
            <div class="backgroundImage view">
                <div>
                    <img src="./resources/IMG/banners/breakfast.jpg" alt="Breakfast Image"/>
                </div>
            </div>
            <div class="mist">
                <div>
                    <div>
                        <h1>Content Page</h1>
                    </div>
			    </div>
            </div>
		</div>
		<nav>
			<div class="closeMenu">
				<a href="#" class="closeMenu">
					<div>
						<div>
							<span>X</span><!-- Style with CSS -->
						</div>
					</div>
				</a>
			</div>
			<ul>
                <li><a href="./">Home</a></li>
                <li><a href="./mall.php">Malls</a></li>
				<li><a href="./office_lunches.php">Office Lunches</a></li>
				<li><a href="./contact.php">Contact Us</a></li>
			</ul>
		</nav>
		<div class="content">
			<div class="row">
				<article class="column xsmall-12 large-8 large-left pageData">
					<!-- Page content will be placed here -->
					<h2>This is the menu style</h2>
                    <dl class="menu">
                        <dt>
                            <div class="border">
                                <h4>Spicy Meatballs</h4>
                            </div>
                        </dt>
                        <dd>
                            <p>Marinated minced lamb cooked in a rich yoghurt sauce, layered on fresh crispy salads and showered with coriander leaves and roasted onions.</p>
                        </dd>
                        <dt>
                            <div class="border">
                                <h4>Chilli Infused Chicken</h4>
                            </div>
                        </dt>
                        <dd>
                            <p>Chicken Roasted in a light garlic and Chilli infused marinade served on a bed of onion and spinich finished with a light and spicy sauce.</p>
                        </dd>
                        <dt>
                            <div class="border">
                                <h4>Skewered Lamb &amp; Vegetables</h4>
                            </div>
                        </dt>
                        <dd>
                            <p>Wholesome chunks of lamb marinated in herbs & spices  laid  on the finest vegetables Laced with a delicate variation of sauces.</p>
                        </dd>
                        <dt>
                            <div class="border">
                                <h4>Roast Beef</h4>
                            </div>
                        </dt>
                        <dd>
                            <p>Served on a bed of roast potatoes and red onions an laced with the finest English mustard.</p>
                        </dd>
                        <dt>
                            <div class="border">
                                <h4>Prawns Prepared in a Potato Wrap</h4>
                            </div>
                        </dt>
                        <dd>
                            <p>Laced with a yoghurt, mint & cucumber dressing.</p>
                        </dd>
                        <dt>
                            <div class="border">
                                <h4>Roasted Vegetables</h4>
                            </div>
                        </dt>
                        <dd>
                            <p>Delicately roasted  in herbs and married with the finest haloumi cheese.</p>
                        </dd>
                        <dt>
                            <div class="border">
                                <h4>Bombay Potato Wrap</h4>
                            </div>
                        </dt>
                        <dd>
                            <p>Potatoes cooked the Bombay style  in light olive oil, lightly spiced with chilly, turmeric and o poppy seeds.</p>
                        </dd>
                    </dl>

				</article>
                <div class="column xsmall-12 large-4">
                    <aside class="sideContent">
                        <p>Just some side content for the website</div>
                    </aside>
                </div>
			</div>
		</div>
		<footer>
			<div class="row">
				<div class="column xsmall-12 large-6">
					<a href="./" class="logo logo-footer">
						<span>The Peppercorn Grill Limited</span>
					</a>
					<p>&copy; The Peppercorn Grill Limited 2018</p>
					<p>Company Registration: 10890159</p>
				</div>
				<div class="column xsmall-12 xsmall-hide large-6 large-show large-right">
					<!-- Smaller navigation and  social media-->
					<nav class="smaller">
						<ul>
                            <li><a href="./">Home</a></li>
                            <li><a href="./mall.php">Malls</a></li>
                            <li><a href="./office_lunches.php">Office Lunches</a></li>
                            <li><a href="./contact.php">Contact Us</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</footer>
	</body>
</html>
