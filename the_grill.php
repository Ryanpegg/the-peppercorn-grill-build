<?php
$pageName = 'The Grill';
$pageFile = 'the_grill';
$description = '';
$keywords = '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title><?php print($pageName); ?></title>
    <meta name="desscription" content="<?php print($description); ?>"/>
    <meta name="keywords" content="<?php print($keywords); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?php
    include_once './resources/pages/content/style_and_script.php';
    ?>
</head>
<body>
<?php
// Include the header for the pages
include_once './resources/pages/content/header.php';
// Include the banner
include_once './resources/pages/content/banner.php';
// Include the navigation
include_once './resources/pages/content/navigation.php';
// Include the content section
include_once './resources/pages/content/content.php';
//Include the footer
include_once './resources/pages/content/footer.php';
?>
</body>
</html>
